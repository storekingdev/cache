"use strict;"
var crypto = require("crypto");
var events = require('events');
var crudder = null;
/* -------------------------------------------------REDIS-SETUP----------------------------------------------------------------------------------- */
var host = process.env.REDIS_CON.split(":")[0];
var port = process.env.REDIS_CON.split(":")[1];
const redis = require('redis');
const client = redis.createClient({ port: port, host: host });
/* ----------------------------------------------------------------------------------------------------------------------------------------------- */

function fromCache(req, module_name) {
    var eventEmitter = new events.EventEmitter();
    var params = mapper(req);
    var url = req.url;
    var hashParams = generateHash({ url: url, reqParams: params });

    client.get(`${module_name}_${hashParams}`, (err, cachedResult) => {
        if (cachedResult) {
            eventEmitter.emit('data', JSON.parse(cachedResult));
        } else {
            eventEmitter.emit('empty');
        }
    });

    return eventEmitter;
}


function persistCache(req, data, module_name, expiry) {
    var eventEmitter = new events.EventEmitter();
    var params = mapper(req);
    var url = req.url;
    var hashParams = generateHash({ url: url, reqParams: params });

    client.setex(`${module_name}_${hashParams}`, expiry, JSON.stringify(data), function (error) {
        if (error) {
            eventEmitter.emit('error');
        } else {
            eventEmitter.emit('success');
        }
    });

    return eventEmitter;
}

function invalidateCache() {

}

function generateHash(params) {
    return crypto.createHash('md5').update(JSON.stringify(params)).digest('hex');
}


var mapper = (req) =>
    Object.keys(req.swagger.params).reduce((prev, curr) => {
        prev[curr] = req.swagger.params[curr].value;
        return prev;
    }, {});


module.exports = {
    fromCache: fromCache,
    persistCache: persistCache
}